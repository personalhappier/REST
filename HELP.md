### Guides
The following guides illustrate how to use some features concretely:

If you use Maven, you can run the application by using ./mvnw spring-boot:run. 
Alternatively, you can build the JAR file with ./mvnw clean package and then run the JAR file, as follows:

java -jar target/REST-1.0-SNAPSHOT.war

####Use swagger docs

`Option1:` http://localhost:5000/swagger-ui/

`Option2:` http://localhost:5000/swagger-ui/index.html

####Use in-memory DB

After starting the application, navigate to http://localhost:5000/h2-console,
 which will present us with a login page
 
 In application.properties file:
 
 `Option1:`
 spring.datasource.url=jdbc:h2:mem:testdb 
 data will be lost when we restart the application
 
 `Option2:`
 spring.datasource.url=jdbc:h2:file:/data/rest
 data stores in specified file
 
 