package com.rest.model;

public class Habit {
    long id;
    String name;
    long category_id;
    long created_by;

    public Habit() {
    }

    public Habit(long id, String name, long category_id, long created_by) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.created_by = created_by;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }
}
