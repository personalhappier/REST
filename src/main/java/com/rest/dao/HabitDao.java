package com.rest.dao;

import com.rest.model.Habit;
import java.util.List;

public interface HabitDao {
    List<Habit> getAll();
    Habit getById(long id);
    void update(Habit habit);
    long insert(Habit habit);
    void delete(long id);

}
