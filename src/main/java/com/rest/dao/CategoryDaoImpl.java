package com.rest.dao;

import com.rest.model.Category;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class CategoryDaoImpl implements CategoryDao {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private final String sqlForGetAll = "select * from category";


    public List<Category> getAll() {
        BeanPropertyRowMapper<Category> rowMapper = new BeanPropertyRowMapper<>(Category.class);
        return jdbcTemplate.query(sqlForGetAll, rowMapper);
    }


}
