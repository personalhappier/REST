package com.rest.dao;

import com.rest.model.Habit;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class HabitDaoImpl implements HabitDao {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static final BeanPropertyRowMapper<Habit> ROW_MAPPER = new BeanPropertyRowMapper<>(Habit.class);

    @Override
    public List<Habit> getAll() {
        String sqlForGetAll = "select * from habit";
        return namedParameterJdbcTemplate.query(sqlForGetAll, ROW_MAPPER);
    }

    @Override
    public Habit getById(long id) {
        String sqlForGetAll = "select * from habit where id = :id";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("id", id);
        return namedParameterJdbcTemplate.queryForObject(sqlForGetAll, param, ROW_MAPPER);
    }

    @Override
    public void update(Habit habit) {
        String sql = "update  habit set name = :name, category_id = :category_id, created_by = :created_by where id=:id";
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(habit);
        namedParameterJdbcTemplate.update(sql, params);
    }

    @Override
    public long insert(Habit habit) {
        String sql = "insert into habit(name, category_id, created_by) values (:name, :category_id, :created_by)";
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(habit);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(sql, params, keyHolder);
        return (long)keyHolder.getKey();
    }

    @Override
    public void delete(long id) {
        String sql = "delete from habit where id=:id";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("id", id);
        namedParameterJdbcTemplate.update(sql, param);
    }
}
