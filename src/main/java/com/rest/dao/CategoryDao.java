package com.rest.dao;

import com.rest.model.Category;
import java.util.List;

public interface CategoryDao {
    List<Category> getAll();
}
