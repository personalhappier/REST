package com.rest.controllers;

import com.rest.model.Habit;
import com.rest.dao.HabitDao;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/habits")
public class HabitController {
    @Autowired
    HabitDao habitDao;

    @GetMapping
    public List<Habit> getAll() {
        return habitDao.getAll();
    }
    @GetMapping("/{id}")
    public Habit getById(@PathVariable long id) {
        return habitDao.getById(id);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping @ResponseStatus(HttpStatus.CREATED)
    public long insert(@RequestBody Habit habit) {
        return habitDao.insert(habit);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping()
    public void update(@RequestBody Habit habit ) {
         habitDao.update(habit);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("{id}")
    public void delete(@PathVariable long id) {
        habitDao.delete(id);
    }

}
