package com.rest.controllers;

import com.rest.model.Category;
import com.rest.dao.CategoryDao;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/categories")
public class CategotyController {
    @Autowired
    private CategoryDao categoryDao;

    @GetMapping()
    public List<Category> getAll() {
        return categoryDao.getAll();
    }
}
