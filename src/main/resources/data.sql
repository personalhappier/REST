--
create table if not exists category
(id bigint auto_increment,
name varchar(255),
primary key (id),
unique (name)
);
insert into category(name) values ('Spiritual');
insert into category(name) values ('Sport');
insert into category(name) values ('Food');
insert into category(name) values ('Study');
insert into category(name) values ('Emotional');
insert into category(name) values ('Health');
insert into category(name) values ('Other');
--
create table if not exists user
(id bigint auto_increment,
name varchar(255),
primary key (id),
unique (name)
);
insert into user(name) values ('Nastya');
insert into user(name) values ('Nastya1');

create table if not exists habit
(id bigint auto_increment,
name varchar(255),
category_id bigint,
created_by bigint,
primary key (id),
unique (name),
foreign key (category_id) references category(id),
foreign key (created_by) references user(id)
);
insert into habit(name, category_id, created_by) values ('hobby', 1, 1);